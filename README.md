# Step Project Forkio
Click [here](https://adkanor.gitlab.io/step-project-forkio/) to see demo-version. 
## Technologies used in this project:

1. HTML
2. CSS
3. SCSS
4. JS
5. Gitlab
6. Gulp project assembly with plugins:
    - gulp
    - gulp-sass
    - browser-sync
    - gulp-uglify
    - gulp-imagemin
    - gulp-autoprefixer
    - gulp-cssnano
    - gulp-plumber
    - gulp-rename
    - gulp-rigger
    - gulp-watch
    
##  Project participants:

- Kateryna Mordovtseva
- Oksana Korotich

## Tasks completed by each of the participants:

### *Kateryna Mordovtseva*

- Creating a remote repository and place the project on the Internet using Gitlab pages.
- Layout of the site header with the top menu (including the drop-down menu at low screen resolution).
- Layout of the People Are Talking About Fork section.
- Сreating gulpfile.js and setting it up.

### *Oksana Korotich*


- Layout block Revolutionary Editor.
- Layout  Here is what you get section.
- Layout  Fork Subscription Pricing section.
- Create a readme file.
